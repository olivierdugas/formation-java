# Formation-Java

## What do I need to get started?

- Git
- Eclipse with infinitest

## How do I start the tests?

```
$ git clone
$ import one of the projects into eclipse
$ run as junit tests
```

## How do I use JUnit and Mockito?

Have a look at the cheatsheets directory. There are some old, but valid cues that can help.

Also have a look at Hamcrest tests assertions:
- https://code.google.com/archive/p/hamcrest/wikis/Tutorial.wiki

