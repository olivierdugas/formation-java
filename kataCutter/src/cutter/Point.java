package cutter;

public class Point {
	public final double x;
	public final double y;
	
	public Point(double x, double y){
		this.x = x;
		this.y = y;
	}

	public boolean isSame(Point backtrackTo) {
		return x == backtrackTo.x && y == backtrackTo.y;
	}
}
