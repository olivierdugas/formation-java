package cutter;

public interface CuttingMachine {

	void cut(double direction, double length);

}
