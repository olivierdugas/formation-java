package cutter;

public class TrajectoryCalculator {

	public double findDirection(Point from, Point to) {
		Point difference = new Point(to.x - from.x, to.y - from.y);
		return Math.atan2(difference.y, difference.x);
	}

	public double findDistance(Point from, Point to) {
		return Math.sqrt(Math.pow(to.x - from.x, 2) + Math.pow(to.y - from.y, 2));
	}
	
	public boolean isPointBetween(Point point1, Point point2, Point current){
		//http://stackoverflow.com/questions/11907947/how-to-check-if-a-point-lies-on-a-line-between-2-other-points
		if (!arePointsOnTheSameLine(point1, point2, current))
			return false;

		final double deltaX = point2.x - point1.x;
		final double deltaY = point2.y - point1.y;
		
		if (Math.abs(deltaX) >= Math.abs(deltaY)) {
			return deltaX > 0 ? 
			    point1.x <= current.x && current.x <= point2.x :
			    point2.x <= current.x && current.x <= point1.x;
		} else {
			return deltaY > 0 ? 
			    point1.y <= current.y && current.y <= point2.y :
			    point2.y <= current.y && current.y <= point1.y;
		}
	}
	
	public boolean arePointsOnTheSameLine(Point p1, Point p2, Point p3){
		final double deltaX1 = p3.x - p1.x;
		final double deltaY1 = p3.y - p1.y;

		final double deltaX2 = p2.x - p1.x;
		final double deltaY2 = p2.y - p1.y;

		final double cross = deltaX1 * deltaY2 - deltaY1 * deltaX2;
		return cross == 0;
	}

}
