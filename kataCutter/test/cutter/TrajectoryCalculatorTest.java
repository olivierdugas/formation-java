package cutter;

import org.junit.Test;
import static org.junit.Assert.*;

public class TrajectoryCalculatorTest {
	private TrajectoryCalculator calculator = new TrajectoryCalculator();
	private double epsilon = 1e-7;
	
	@Test
	public void givenHorizontalPoints_WhenFindDirection_ThenShouldReturn0Rad(){
		Point from = new Point(0,0);
		Point to = new Point(1,0);
		
		double direction = calculator.findDirection(from, to);

		double expected = 0;
		assertEquals(expected, direction, epsilon);
	}
	
	@Test
	public void givenReverseHorizontalPoints_WhenFindDirection_ThenShouldReturnPi(){
		Point from = new Point(1,0);
		Point to = new Point(0,0);
		
		double direction = calculator.findDirection(from, to);

		double expected = Math.PI;
		assertEquals(expected, direction, epsilon);
	}
	
	@Test
	public void givenVerticalPoints_WhenFindDirection_ThenShouldReturnHalfPi(){
		Point from = new Point(0,0);
		Point to = new Point(0,1);
		
		double direction = calculator.findDirection(from, to);

		double expected = Math.PI/2;
		assertEquals(expected, direction, epsilon);
	}
	
	@Test
	public void givenReverseVerticalPoints_WhenFindDirection_ThenShouldReturnMinusHalfPi(){
		Point from = new Point(0,1);
		Point to = new Point(0,0);
		
		double direction = calculator.findDirection(from, to);

		double expected = -Math.PI/2;
		assertEquals(expected, direction, epsilon);
	}
	
	@Test
	public void givenAnyTwoPoints_WhenFindDistance_ThenShouldComputeUsualSqrt(){
		Point from = new Point(1,2);
		Point to = new Point(3,5);
		
		double distance = calculator.findDistance(from, to);
		
		double expected = Math.sqrt(Math.pow(to.x - from.x, 2) + Math.pow(to.y - from.y, 2));
		assertEquals(expected, distance, epsilon);
	}
	
	@Test
	public void givenThreePointsOnSameLine_WhenCheckIfSameLine_ThenShouldReturnTrue(){
		Point p1 = new Point(1,2);
		Point p2 = new Point(3,4);
		Point p3 = new Point(5,6);
		
		boolean onTheSameLine = calculator.arePointsOnTheSameLine(p1, p2, p3);
		
		assertTrue(onTheSameLine);
	}
	
	@Test
	public void givenThreePointsTriangle_WhenCheckIfSameLine_ThenShouldReturnFalse(){
		Point p1 = new Point(1,2);
		Point p2 = new Point(3,4);
		Point p3 = new Point(2,5);
		
		boolean onTheSameLine = calculator.arePointsOnTheSameLine(p1, p2, p3);
		
		assertFalse(onTheSameLine);
	}
	
	@Test
	public void givenPointBetweenTwoOthers_WhenCheckIfBetween_ThenShouldReturnTrue(){
		Point p1 = new Point(1,2);
		Point current = new Point(3,4);
		Point p2 = new Point(5,6);
		
		boolean onTheSameLine = calculator.isPointBetween(p1, p2, current);
		
		assertTrue(onTheSameLine);
	}
	
	@Test
	public void givenPointOutsideTwoOthers_WhenCheckIfBetween_ThenShouldReturnFalse(){
		Point p1 = new Point(1,2);
		Point p2 = new Point(3,4);
		Point current = new Point(5,6);
		
		boolean onTheSameLine = calculator.isPointBetween(p1, p2, current);
		
		assertFalse(onTheSameLine);
	}
}
